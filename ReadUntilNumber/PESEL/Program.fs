﻿// Learn more about F# at http://fsharp.org
//https://pl.spoj.com/problems/JPESEL/
open System
let valid (pesel:int []) : bool =
    let result = pesel.[0] + pesel.[1] * 3 + pesel.[2] * 7 + pesel.[3] * 9 + pesel.[4] + pesel.[5] * 3 + pesel.[6] * 7 + pesel.[7] * 9 + pesel.[8] + pesel.[9] * 3 + pesel.[10]
    let resultString = result.ToString()
    match resultString.[resultString.Length - 1] with
    | '0' -> true
    | _ -> false

let charToInt c = 
    int c - int '0'

let boolToDN value =
    match value with
    | true -> 'D'
    | false -> 'N'

let execute readValue = 
    for i = 1 to readValue do
        let pesel = Console.ReadLine() |> Seq.toArray |> Array.map(fun x -> charToInt x)
        let isVaild = valid pesel
        Console.WriteLine(boolToDN isVaild)

[<EntryPoint>]  
let main argv =
    let T = Console.ReadLine()
    let numCases = Int32.Parse(T)
    execute numCases
    0