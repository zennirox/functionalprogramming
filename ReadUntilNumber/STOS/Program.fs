﻿// Learn more about F# at http://fsharp.org
//https://pl.spoj.com/problems/STOS/
open System

// Define an immutable stack
type ImmutableStack<'T> =
  | Empty 
  | Stack of 'T * ImmutableStack<'T>

  member s.Push x = Stack(x, s)

  member s.Pop() = 
    match s with
    | Empty -> failwith "Underflow"
    | Stack(t,_) -> t

  member s.Top() = 
    match s with
    | Empty -> failwith "Contain no elements"
    | Stack(_,st) -> st

  member s.IEmpty = 
    match s with
    | Empty -> true
    | _ -> false

  member s.All() = 
    let rec loop acc = function
    | Empty -> acc
    | Stack(t,st) -> loop (t::acc) st
    loop [] s

let rec readStack number stack =
    match number with
    | "+" -> 0
    | "-" -> 0
    | _   -> Console.WriteLine(number)
             readStack(Console.ReadLine() stack)

[<EntryPoint>]
let main argv = 
    let stack = ImmutableStack.Empty
    readStack Console.ReadLine() stack
    0
    