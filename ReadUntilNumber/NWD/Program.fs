﻿// Learn more about F# at http://fsharp.org
//https://pl.spoj.com/problems/PP0501A/
open System
let modulo a b =
    a % b

let rec nwd a b : double =
    let result = modulo a b
    match result with
    | 0.0 -> b
    | _ -> nwd b result
        

let execute readValue = 
    for i = 1 to readValue do
        let listValue = Console.ReadLine().Split(' ') |> Array.map(fun x -> Double.Parse(x)) |> Array.sort
        let result = nwd listValue.[0] listValue.[1]
        Console.WriteLine(result)

[<EntryPoint>]  
let main argv =
    let T = Console.ReadLine()
    let numCases = Int32.Parse(T)
    execute numCases
    0

