﻿// Learn more about F# at http://fsharp.org
// https://pl.spoj.com/problems/PICIRC/

open System
let addToList list elementToAdd = 
    elementToAdd::list

let parseXYZ (sxyr:string) = 
    let list = sxyr.Split(' ') |> Array.map(fun x -> Int32.Parse(x))
    list.[0], list.[1], list.[2]

let readInput (numCases:int) :List<List<int>> = 
    let mutable mainList = []
    for i = 1 to numCases do 
        let positions = Console.ReadLine().Split(' ') |> Array.map(fun x -> Int32.Parse(x)) |> Array.toList
        mainList <- addToList mainList positions
    mainList

        
[<EntryPoint>]
let main argv =
    let sxyr = Console.ReadLine()
    let t = Console.ReadLine()
    let numCases = Int32.Parse(t);
    
    let x,y,z = parseXYZ sxyr
    let allInputs = readInput numCases


    0 // return an integer exit code
