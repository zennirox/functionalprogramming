﻿open System

let rec readUntilFound number =
    match number with
    | "42" -> 0
    | _    -> Console.WriteLine(number)
              readUntilFound(Console.ReadLine())

[<EntryPoint>]
let main argv = 
    readUntilFound(Console.ReadLine())
    
